-- MySQL dump 10.13  Distrib 5.7.35, for Linux (x86_64)
--
-- Host: localhost    Database: pam
-- ------------------------------------------------------
-- Server version	5.7.35-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `faction` int(11) NOT NULL,
  `controlable` tinyint(1) NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `claimable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `award`
--

DROP TABLE IF EXISTS `award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `abreviation` varchar(20) NOT NULL,
  `image` varchar(50) NOT NULL,
  `faction` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_unit`
--

DROP TABLE IF EXISTS `base_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permanent` tinyint(1) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `forum_group_id` varchar(45) DEFAULT NULL,
  `forum_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `briefing`
--

DROP TABLE IF EXISTS `briefing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `briefing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mission_id` int(11) NOT NULL,
  `faction` int(11) NOT NULL,
  `text` varchar(30000) DEFAULT NULL,
  PRIMARY KEY (`id`,`mission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=449 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `is_primary` tinyint(1) NOT NULL,
  `platform` int(11) NOT NULL,
  `campaign_status` int(10) NOT NULL,
  `time` varchar(45) NOT NULL,
  `open` tinyint(1) NOT NULL,
  `campaign_link` int(11) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `career_character`
--

DROP TABLE IF EXISTS `career_character`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `career_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personified_by` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `portrait_seed` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`,`personified_by`)
) ENGINE=InnoDB AUTO_INCREMENT=6127 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim`
--

DROP TABLE IF EXISTS `claim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `accepted` tinyint(1) NOT NULL,
  `accepted_by` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`,`report_id`,`asset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16895 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_ground`
--

DROP TABLE IF EXISTS `claim_ground`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_ground` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`,`claim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14543 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_lw`
--

DROP TABLE IF EXISTS `claim_lw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_lw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` int(11) NOT NULL,
  `claim_time` varchar(5) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `opponent` varchar(20) DEFAULT NULL,
  `type_of_destruction` int(11) NOT NULL,
  `type_of_impact` int(11) NOT NULL,
  `fate_of_crew` int(11) NOT NULL,
  `witness` int(11) DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`,`claim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4368 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_ra`
--

DROP TABLE IF EXISTS `claim_ra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_ra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` int(11) NOT NULL,
  `enemy_status` int(11) NOT NULL,
  `shared` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`,`claim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_raf`
--

DROP TABLE IF EXISTS `claim_raf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_raf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` int(11) NOT NULL,
  `enemy_status` int(11) NOT NULL,
  `shared` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`claim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8012 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `claim_vvs`
--

DROP TABLE IF EXISTS `claim_vvs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `claim_vvs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` int(11) NOT NULL,
  `claim_time` varchar(5) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `witness` int(11) DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `group_claim` varchar(45) NOT NULL,
  PRIMARY KEY (`id`,`claim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2917 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `comment_date` date NOT NULL,
  `comment_text` varchar(500) NOT NULL,
  PRIMARY KEY (`id`,`report_id`,`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7049 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `decoration`
--

DROP TABLE IF EXISTS `decoration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decoration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `character_id` int(11) NOT NULL,
  `award_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `awarded` tinyint(1) NOT NULL,
  `awarded_by` int(11) DEFAULT NULL,
  `recommendation_date` date NOT NULL,
  PRIMARY KEY (`id`,`character_id`,`award_id`),
  UNIQUE KEY `characterID_3` (`character_id`,`award_id`),
  KEY `characterID` (`character_id`,`award_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104776 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deployed_unit`
--

DROP TABLE IF EXISTS `deployed_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deployed_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `base_unit_id` int(11) DEFAULT NULL,
  `hist_unit_id` int(11) NOT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `unit_type` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  PRIMARY KEY (`id`,`campaign_id`,`hist_unit_id`),
  KEY `fk_deployedunits_acgunits1_idx` (`base_unit_id`),
  KEY `fk_deployed_unit_asset1_idx` (`asset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=448 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hist_unit`
--

DROP TABLE IF EXISTS `hist_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hist_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `faction` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `original_unit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_url` varchar(200) NOT NULL,
  `acg_member` varchar(45) NOT NULL,
  `media_type` varchar(45) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `date_submitted` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `callsign` varchar(20) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `map_viewer` tinyint(1) DEFAULT NULL,
  `mission_builder` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=694 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_roster_asset`
--

DROP TABLE IF EXISTS `member_roster_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_roster_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `roster_asset_id` int(11) NOT NULL,
  `base_unit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `member_status_log`
--

DROP TABLE IF EXISTS `member_status_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_status_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `member_status` int(11) NOT NULL,
  `status_date_in` date NOT NULL,
  `status_date_out` date DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`,`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2306 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mission`
--

DROP TABLE IF EXISTS `mission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `campaign_id` int(11) NOT NULL,
  `real_date` date NOT NULL,
  `hist_date` datetime NOT NULL,
  `mission_status` int(10) NOT NULL,
  `front_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`campaign_id`)
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `namepool`
--

DROP TABLE IF EXISTS `namepool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `namepool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `type` varchar(10) NOT NULL,
  `faction` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3917 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promotion`
--

DROP TABLE IF EXISTS `promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `promotion_date` date NOT NULL,
  `rank_value` int(11) NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`,`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2179 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rank`
--

DROP TABLE IF EXISTS `rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `abreviation` varchar(50) NOT NULL,
  `value` int(11) NOT NULL,
  `faction` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `forum_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index2` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mission_id` int(11) NOT NULL COMMENT '		',
  `character_id` int(11) NOT NULL,
  `deployed_unit_id` int(11) NOT NULL,
  `base` varchar(50) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `markings` varchar(20) NOT NULL,
  `synopsis` varchar(15000) DEFAULT NULL,
  `asset_status` int(11) NOT NULL,
  `pilot_status` int(11) NOT NULL,
  `date_submitted` date NOT NULL,
  `accepted` tinyint(1) NOT NULL,
  `accepted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`mission_id`,`character_id`,`deployed_unit_id`,`asset_id`),
  KEY `missionID` (`mission_id`),
  KEY `authorID` (`character_id`),
  KEY `squadronID` (`deployed_unit_id`),
  KEY `acceptedBy` (`accepted_by`),
  KEY `fk_report_asset1_idx` (`asset_id`),
  CONSTRAINT `fk_report_acg_member1` FOREIGN KEY (`accepted_by`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_report_asset1` FOREIGN KEY (`asset_id`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reports_careercharacters1` FOREIGN KEY (`character_id`) REFERENCES `career_character` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reports_deployedunits1` FOREIGN KEY (`deployed_unit_id`) REFERENCES `deployed_unit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reports_missions1` FOREIGN KEY (`mission_id`) REFERENCES `mission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25504 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_detail_lw`
--

DROP TABLE IF EXISTS `report_detail_lw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_detail_lw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `swarm` int(11) NOT NULL,
  `swarm_pos` int(11) NOT NULL,
  PRIMARY KEY (`id`,`report_id`),
  KEY `fk_reportdetailslw_reports1_idx` (`report_id`),
  CONSTRAINT `fk_reportdetailslw_reports1` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12035 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_detail_ra`
--

DROP TABLE IF EXISTS `report_detail_ra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_detail_ra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `sezione` int(11) NOT NULL,
  `sezione_pos` int(11) NOT NULL,
  PRIMARY KEY (`id`,`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_detail_raf`
--

DROP TABLE IF EXISTS `report_detail_raf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_detail_raf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `flight` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `section_pos` int(11) NOT NULL,
  `serial_no` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`,`report_id`),
  KEY `fk_reportdetailsraf_reports1_idx` (`report_id`),
  CONSTRAINT `fk_reportdetailsraf_reports1` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7952 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_lw`
--

DROP TABLE IF EXISTS `report_lw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_lw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `swarm` int(11) NOT NULL,
  `swarm_pos` int(11) NOT NULL,
  PRIMARY KEY (`id`,`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_raf`
--

DROP TABLE IF EXISTS `report_raf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_raf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `flight` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `section_pos` int(11) NOT NULL,
  `serial_no` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`,`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_response`
--

DROP TABLE IF EXISTS `report_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `member_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1368 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roster`
--

DROP TABLE IF EXISTS `roster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `squadronID` int(11) NOT NULL,
  `positionID` int(11) NOT NULL,
  `memberID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=529 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roster_asset`
--

DROP TABLE IF EXISTS `roster_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roster_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `hist_unit_id` int(11) NOT NULL,
  `markings` varchar(45) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transfer`
--

DROP TABLE IF EXISTS `transfer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `base_unit_id` int(11) NOT NULL,
  `transfer_date_in` date NOT NULL,
  `transfer_date_out` date DEFAULT NULL,
  PRIMARY KEY (`id`,`member_id`,`base_unit_id`),
  KEY `fk_transfers_acgunits1` (`base_unit_id`),
  KEY `fk_transfer_acg_member1_idx` (`member_id`),
  CONSTRAINT `fk_transfer_acg_member1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transfers_acgunits1` FOREIGN KEY (`base_unit_id`) REFERENCES `base_unit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1611 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-15  9:57:42
