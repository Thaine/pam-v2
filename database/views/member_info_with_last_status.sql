CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `member_info_with_last_status` AS
    SELECT 
        `member`.`id` AS `member_id`,
        `member`.`username` AS `username`,
        `member`.`callsign` AS `callsign`,
        `member`.`admin` AS `admin`,
        `member_max_status`.`member_status` AS `member_status`,
        `member_max_transfer`.`base_unit_id` AS `base_unit_id`
    FROM
        ((`member`
        LEFT JOIN `member_max_status` ON ((`member_max_status`.`member_id` = `member`.`id`)))
        LEFT JOIN `member_max_transfer` ON ((`member_max_transfer`.`member_id` = `member`.`id`)))
    ORDER BY `member`.`username`