CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `character_decoration` AS
    SELECT 
        `decoration`.`id` AS `id`,
        `decoration`.`character_id` AS `character_id`,
        `decoration`.`award_id` AS `award_id`,
        `decoration`.`awarded` AS `awarded`,
        `decoration`.`date` AS `date`,
        `decoration`.`recommendation_date` AS `recommendation_date`,
        `career_character`.`first_name` AS `first_name`,
        `career_character`.`last_name` AS `last_name`,
        `career_character`.`personified_by` AS `personified_by`,
        `member`.`callsign` AS `callsign`,
        `award`.`name` AS `name`,
        `mission_member_rank`.`abreviation` AS `abreviation`,
        `transfer`.`base_unit_id` AS `base_unit_id`
    FROM
        ((((((((`decoration`
        LEFT JOIN `career_character` ON ((`decoration`.`character_id` = `career_character`.`id`)))
        LEFT JOIN `transfer` ON ((`career_character`.`personified_by` = `transfer`.`member_id`)))
        LEFT JOIN `member` ON ((`member`.`id` = `career_character`.`personified_by`)))
        LEFT JOIN `award` ON ((`award`.`id` = `decoration`.`award_id`)))
        LEFT JOIN `character_latest_mission_date` ON ((`character_latest_mission_date`.`character_id` = `career_character`.`id`)))
        LEFT JOIN `mission` ON ((`mission`.`real_date` = `character_latest_mission_date`.`max_mission_date`)))
        LEFT JOIN `mission_member_faction` ON (((`mission_member_faction`.`mission_id` = `mission`.`id`)
            AND (`mission_member_faction`.`member_id` = `member`.`id`))))
        LEFT JOIN `mission_member_rank` ON (((`mission_member_rank`.`mission_id` = `mission`.`id`)
            AND (`mission_member_rank`.`member_id` = `member`.`id`)
            AND (`mission_member_rank`.`faction` = `mission_member_faction`.`faction`))))
    WHERE
        ((`transfer`.`transfer_date_in` <= `decoration`.`recommendation_date`)
            AND ((`decoration`.`recommendation_date` < `transfer`.`transfer_date_out`)
            OR ISNULL(`transfer`.`transfer_date_out`)))