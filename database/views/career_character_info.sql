CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `career_character_info` AS
    SELECT 
        `career_character`.`id` AS `character_id`,
        `member`.`id` AS `member_id`,
        CONCAT(`career_character`.`first_name`,
                ' "',
                `member`.`callsign`,
                '" ',
                `career_character`.`last_name`) AS `name`
    FROM
        (`career_character`
        LEFT JOIN `member` ON ((`member`.`id` = `career_character`.`personified_by`)))
    ORDER BY CONCAT(`career_character`.`first_name`,
            ' "',
            `member`.`callsign`,
            '" ',
            `career_character`.`last_name`)